from textual.app import App, ComposeResult
from textual.containers import ScrollableContainer
from textual.widgets import Button, Footer, Header, Static, Input, Label
import math
import struct
import serial

import socket
TCP_IP = "192.168.0.100"
# TEST_IP = "127.0.0.6"
PORT = 12



class agv_command:
    # type = 0x01
    # id = 0x0c
    # mode = 0x03    0x01 for movement 0x03 for stop
    pos = math.nan
    # vel = -5.0
    trq = 0.0
    acc = math.nan

    def __init__(self, type, id, mode, vel):
        self.type = type
        self.id = id
        self.mode = mode
        self.vel = vel

    def get_bytes(self):
        ba_pos = bytearray(struct.pack("f", self.pos))
        ba_vel = bytearray(struct.pack("f", self.vel))
        ba_trq = bytearray(struct.pack("f", self.trq))
        ba_acc = bytearray(struct.pack("f", self.acc))
        ba_arb = bytearray([self.type,self.id,self.mode])  
        byte_array = ba_arb+ba_pos+ba_vel+ba_trq+ba_acc
        return byte_array
    
class Motor(Static):
    vel = 0.0
    motor_id = 0x00
    """A stopwatch widget."""
    def compose(self) -> ComposeResult:
        yield Label("Motor "+str(self.id),id="MotorHead")
        yield Button("Start", id="start", variant="success")
        yield Input(placeholder="type desired velocity",id="vel_input",type="number")
        yield Button("Stop", id="stop", variant="error")

    def on_button_pressed(self, event: Button.Pressed) -> None:
        """Event handler called when a button is pressed."""
        
        if event.button.id == "start":
            print("starting")
            print(self.vel)
            print(hex(int(self.id[1:])))
            out_command = agv_command(0x01,int(self.id[1:]),0x01,self.vel)
            send_to_stm(out_command)
            
        elif event.button.id == "stop":
            print("stopping")
            print(self.vel)
            print(hex(int(self.id[1:])))
            out_command = agv_command(0x01,int(self.id[1:]),0x03,self.vel)
            send_to_stm(out_command)
    
    def on_input_changed(self, event: Input.Changed) -> None:
        print(event.input.value)
        try:
            self.vel = float(event.input.value)
        except:
            self.vel = 0.0

class MoteusApp(App):

    CSS_PATH = "moteus_app.tcss"
    BINDINGS = [("d", "toggle_dark", "Toggle dark mode")]

    def compose(self) -> ComposeResult:
        """Create child widgets for the app."""
        yield Header()
        yield Footer()
        yield ScrollableContainer(Motor(id="d10"),Motor(id="d11"),Motor(id="d12"),Motor(id="d13"))

    def action_toggle_dark(self) -> None:
        """An action to toggle dark mode."""
        self.dark = not self.dark

def send_to_stm(to_send: agv_command):
    bytes_to_send = to_send.get_bytes()
    client.send(bytes_to_send)
    print(bytes_to_send)
    

if __name__ == "__main__":
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect((TCP_IP, PORT))
    app = MoteusApp()
    app.run()
