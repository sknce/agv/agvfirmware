import serial
import math
import struct

import socket
# TCP_IP = "192.168.0.6"
TEST_IP = "127.0.0.6"
PORT = 12

class agv_command:
    type = 0x01 #1 byte - Motor/Robot
    id = 0x0b #1 byte - ID of Motor/Robot
    mode = 0x01 #1 byte - depending on type
    pos = math.nan #4 bytes - data fields
    vel = 2.5 #4 bytes
    trq = 1.5 #4 bytes
    acc = math.nan #4 bytes
#total 19 bytes
    def get_bytes(self):
        ba_pos = bytearray(struct.pack("f", self.pos))
        ba_vel = bytearray(struct.pack("f", self.vel))
        ba_trq = bytearray(struct.pack("f", self.trq))
        ba_acc = bytearray(struct.pack("f", self.acc))
        ba_arb = bytearray([self.type,self.id,self.mode])  
        byte_array = ba_arb+ba_pos+ba_vel+ba_trq+ba_acc
        return byte_array

out_command = agv_command()
bytes_to_send = out_command.get_bytes()

client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect((TEST_IP, PORT))
print(bytes_to_send)
client.send(bytes_to_send)