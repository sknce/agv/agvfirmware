import serial
import binascii

ser = serial.Serial()
ser.port = "COM3"
ser.baudrate = 115200
ser.open()
diag_frame = []
diag_frame.append(0x02)
for i in range(18):
    diag_frame.append(0x50)
ser.write(diag_frame)
c = ser.read(109)
print(c)
print(c[0])
print(binascii.hexlify(c[1:28],"-"))
print(binascii.hexlify(c[28:55],"-"))
print(binascii.hexlify(c[55:82],"-"))
print(binascii.hexlify(c[82:109],"-"))
ser.close()

