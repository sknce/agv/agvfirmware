import serial
import math
import struct

class agv_command:
    type = 0x01
    id = 0x0c
    mode = 0x03
    pos = math.nan
    vel = -5.0
    trq = 0.0
    acc = math.nan

    def get_bytes(self):
        ba_pos = bytearray(struct.pack("f", self.pos))
        ba_vel = bytearray(struct.pack("f", self.vel))
        ba_trq = bytearray(struct.pack("f", self.trq))
        ba_acc = bytearray(struct.pack("f", self.acc))
        ba_arb = bytearray([self.type,self.id,self.mode])  
        byte_array = ba_arb+ba_pos+ba_vel+ba_trq+ba_acc
        return byte_array

out_command = agv_command()
bytes_to_send = out_command.get_bytes()
ser = serial.Serial()

ser.port="COM5"
ser.baudrate=115200
ser.open()
if ser.is_open:
    ser.write(bytes_to_send)