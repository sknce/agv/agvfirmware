/*
 * AGV.h
 *
 *  Created on: Nov 28, 2023
 *      Author: maksg
 */

#ifndef SRC_AGV_H_
#define SRC_AGV_H_

#include "moteus.h"
#include "FD.h"
#include "main.h"
#include "structs.h"


class AGV {
public:
	AGV();
	virtual ~AGV();
	moteus motor_1;
	moteus motor_2;
	moteus motor_3;
	moteus motor_4;
	moteus motor_5;
	void init(FDCAN_HandleTypeDef hfdcan, uint32_t ID);
	int command(uint8_t*  in_command);
	void repl(uint8_t *pRxData);
	void query(int motor);
	void interpret(FDCAN_RxHeaderTypeDef head, uint8_t* in_data);
	void resend_all();
private:
	FD can_fd;
	uint8_t ID;
	bool shell_motor_command(uint8_t*  in_command, float* pVel, float* pTorque, uint8_t* pId);
	void shell_command(uint8_t*  in_command, robot_command* out_command);
	void motor_exec();
	void fkine();
	steer_vector ikine(float v_x, float v_y, float rotation);

};

#endif /* SRC_AGV_H_ */
