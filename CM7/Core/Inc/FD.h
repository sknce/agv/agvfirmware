/*
 * FD.h
 *
 *  Created on: Nov 18, 2023
 *      Author: maksg
 */

#ifndef SRC_FD_H_
#define SRC_FD_H_

#include <stdio.h>
#include "main.h"
#include "structs.h"

class FD {
public:
	FD();
	virtual ~FD();
	void bind(FDCAN_HandleTypeDef hfdcan);
	int put_to_tx(uint32_t ID, uint8_t *payload);
	int put_to_tx(can_frame* frame);
	int get_from_rx(FDCAN_TxHeaderTypeDef *header, uint8_t *payload);
	int send_frame(can_frame* in_frame);

private:
	FDCAN_HandleTypeDef hfdcan;
	FDCAN_TxHeaderTypeDef TxHeader;
	FDCAN_RxHeaderTypeDef RxHeader;
};

#endif /* SRC_FD_H_ */
