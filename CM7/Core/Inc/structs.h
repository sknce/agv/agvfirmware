#ifndef INC_STRUCTS_H
#define INC_STRUCTS_H

struct can_frame{
	uint8_t payload[48];
	uint16_t identifier;
};

struct robot_command{
	uint8_t type;
	uint8_t id;
	uint8_t mode;
	// using unions to minimize struct size
	union{
		float pos;
		float v_x;
	};

	union{
		float vel;
		float v_y;
	};

	union{
		float trq;
		float rot;
	};

	float acc;
};

struct steer_vector{
	float wfl;
	float wfr;
	float wrl;
	float wrr;
	float rot;
};

//struct motor_param{
//	float position;
//	float velocity;
//	float torque;
//	float voltage;
//	uint8_t mode;
//	uint16_t fault;
//};

struct motor_param{
	float position;
	float velocity;
	float torque;
	float power;
	float voltage;
	float board_temp;
	uint8_t mode;
	uint16_t fault;
};

struct parameters{
	uint8_t type;
	motor_param motor_1_params;
	motor_param motor_2_params;
	motor_param motor_3_params;
	motor_param motor_4_params;
};

#endif
