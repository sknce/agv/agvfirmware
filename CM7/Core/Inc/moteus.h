/*
 * moteus.h
 *
 *  Created on: Dec 2, 2023
 *      Author: maksg
 */

#ifndef INC_MOTEUS_H_
#define INC_MOTEUS_H_

#include <stdint.h>
#include "structs.h"

class moteus {
public:
	moteus(uint8_t ID);
	virtual ~moteus();
//	void init(uint8_t ID);
	void velocity_payload(float velocity, float torque, uint8_t* hex_frame, uint8_t* identifier); //deprecated
	void velocity_frame(float velocity, float torque, can_frame* frame);
	void stop_frame(can_frame* frame);
	void brake_frame(can_frame* frame);

	void standard_query(can_frame* frame);

	//parameters got from driver
	motor_param params;

	void set_frame(can_frame in_frame);

	bool command_available;
	can_frame last_frame;

	bool is_available();
	void copy_param(uint8_t* dest);
	void log();
private:
	uint8_t ID;
	uint16_t make_identifier(uint8_t type);
	bool driver_connected;

	uint32_t last_message_rcvd;
	uint32_t max_gap;

};


#endif /* INC_MOTEUS_H_ */
