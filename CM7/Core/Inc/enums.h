#ifndef INC_ENUMS_H
#define INC_ENUMS_H

enum error{
	OK = 0,
	FIFO_FULL = 1,
	OTHER = 2
};

enum command_type{
	ROBOT = 0,
	MOTOR = 1,
	PARAM = 2
};

enum command_mode{
	POS = 0,
	VEL = 1,
	STOP = 3,
	BRAKE = 4
};

enum query_moteus{
	QUERY = 0,
	INSTRUCTION = 1,
	COMBINED = 2
};

#endif
