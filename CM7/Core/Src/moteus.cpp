/*
 * moteus.cpp
 *
 *  Created on: Dec 2, 2023
 *      Author: maksg
 */

#include "moteus.h"
#include <cstring>
#include "enums.h"
#include "stm32h7xx_hal.h"

moteus::moteus(uint8_t ID) : ID{ID}{
	this->last_frame = {0};
	this->command_available = false;
	this->driver_connected = false;
	this->last_message_rcvd = 0;
	this->max_gap = 200;
}

moteus::~moteus() {
	// TODO Auto-generated destructor stub
}

void moteus::velocity_payload(float velocity, float torque, uint8_t* payload, uint8_t* identifier){
	//in velocity frame we pass just 3 arguments to moteus:
	// position = nan 0x7FC00000 constant float
	// velocity = variable float limit is set in moteus driver
	// Feedforward torque = variable float
	// with mode commands it adds up to 17 bytes. using 0x50 NOP we can add padding to achieve 24 data bytes

	*(identifier+1) = 0x10;
	*identifier = this->ID;

	uint32_t nan = 0x7fc00000;
	*payload = 0x01;
	*(payload+1) = 0x00;
	*(payload+2) = 0x0A;
	*(payload+3) = 0x0F;
	*(payload+4) = 0x20;
	memcpy(payload+5,&nan,4);
	memcpy(payload+9,&velocity,4);
	memcpy(payload+13,&torque,4); //we know we are inputing floats so we can safely put 4 as size
	uint8_t nop = 0x50;
	int i=0;
	for(i=0;i<=7;i++)
		*(payload+17+i) = nop;

}


void moteus::velocity_frame(float velocity, float torque, can_frame* frame){
	//in velocity frame we pass just 3 arguments to moteus:
	// position = nan 0x7FC00000 constant float
	// velocity = variable float limit is set in moteus driver
	// Feedforward torque = variable float
	// with mode commands it adds up to 17 bytes. using 0x50 NOP we can add padding to achieve 24 data bytes

	frame->identifier = make_identifier(INSTRUCTION);

	uint32_t nan = 0x7fc00000;
	*frame->payload = 0x01;
	*(frame->payload+1) = 0x00;
	*(frame->payload+2) = 0x0A;
	*(frame->payload+3) = 0x0F;
	*(frame->payload+4) = 0x20;
	memcpy(frame->payload+5,&nan,4);
	memcpy(frame->payload+9,&velocity,4);
	memcpy(frame->payload+13,&torque,4); //we know we are inputing floats so we can safely put 4 as size
	uint8_t nop = 0x50;
	int i=0;
	for(i=0;i<7;i++)
		*(frame->payload+17+i) = nop;

}

void moteus::stop_frame(can_frame* frame){
	frame->identifier = make_identifier(INSTRUCTION);

	*frame->payload = 0x01;
	*(frame->payload+1) = 0x00;
	*(frame->payload+2) = 0x00;
	uint8_t nop = 0x50;
	int i=0;
	for(i=0;i<21;i++)
		*(frame->payload+3+i) = nop;

}

void moteus::brake_frame(can_frame* frame){
	frame->identifier = make_identifier(INSTRUCTION);

	*frame->payload = 0x01; //write one register
	*(frame->payload+1) = 0x00; //starting at MODE 0x000
	*(frame->payload+2) = 0x0f; // 15(BRAKE)
	uint8_t nop = 0x50;
	int i=0;
	for(i=0;i<21;i++)
		*(frame->payload+3+i) = nop;

}

//void moteus::standard_query(can_frame* frame){
//	frame->identifier = make_identifier(QUERY);
//	*frame->payload = 0x11; //read one int8 register
//	*(frame->payload+1) = 0x00; //starting at MODE 0x000
//	*(frame->payload+2) = 0x1F; //read three float registers
//	*(frame->payload+3) = 0x01; //starting at POSITION 0x001
//	*(frame->payload+4) = 0x1D; //read one float register
//	*(frame->payload+5) = 0x0D; //starting at VOLTAGE 0x00d
//	*(frame->payload+6) = 0x15; //read one int16 register
//	*(frame->payload+7) = 0x0F; //starting at FAULT 0x00F
//	uint8_t nop = 0x50;
//	int i=0;
//	for(i=0;i<24-8;i++)
//		*(frame->payload+8+i) = nop;
//
//	//this frame reads current MODE, POSITION, VELOCIY, TORQUE, VOLTAGE, FAULT
//}


void moteus::standard_query(can_frame* frame){
	frame->identifier = make_identifier(QUERY);
	*frame->payload = 0x11; //read one int8 register
	*(frame->payload+1) = 0x00; //starting at MODE 0x000
	*(frame->payload+2) = 0x1F; //read three float registers
	*(frame->payload+3) = 0x01; //starting at POSITION 0x001
	*(frame->payload+4) = 0x1D; //read one float register
	*(frame->payload+5) = 0x07; //starting at TEMPERATURE 0x007
	*(frame->payload+6) = 0x1E; //read two float registers
	*(frame->payload+7) = 0x0D; //starting at VOLTAGE 0x00d
	*(frame->payload+8) = 0x15; //read one int16 register
	*(frame->payload+9) = 0x0F; //starting at FAULT 0x00F
	uint8_t nop = 0x50;
	int i=0;
	for(i=0;i<48-10;i++)
		*(frame->payload+10+i) = nop;

	//this frame reads current MODE, POSITION, VELOCIY, TORQUE, VOLTAGE, FAULT
}


uint16_t moteus::make_identifier(uint8_t type){
	switch(type){
	case QUERY:
		return 0x9000|this->ID;
		break;

	case INSTRUCTION:
		return 0x1000|this->ID;
		break;

	case COMBINED:
		return 0x9000|this->ID;
		break;
	}
}

void moteus::set_frame(can_frame in_frame){
	this->last_frame = in_frame;
	this->command_available = true;
}

bool moteus::is_available(){
	if(HAL_GetTick() - this->last_message_rcvd < this->max_gap){
		return true;
	}else{
		return false;
	}
}

void moteus::log(){
	this->last_message_rcvd = HAL_GetTick();
}

