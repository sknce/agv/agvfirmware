/*
 * FD.cpp
 *
 *  Created on: Nov 18, 2023
 *      Author: maksg
 * Description: This class manages CAN FD FIFO queue and is responsible for creating, sending anc receiving CAN FD messages
 */

#include "FD.h"

FD::FD() {
	// TODO Auto-generated constructor stub

}

FD::~FD() {
	// TODO Auto-generated destructor stub
}

void FD::bind(FDCAN_HandleTypeDef hfdcan){
	this->hfdcan = hfdcan;
}

int FD::put_to_tx(uint32_t ID, uint8_t* payload){
	TxHeader.Identifier = ID;
	if(ID>0x7ff){
		TxHeader.IdType = FDCAN_EXTENDED_ID;
	}
	else{
		TxHeader.IdType = FDCAN_STANDARD_ID;
	}
	TxHeader.TxFrameType = FDCAN_DATA_FRAME;
	TxHeader.DataLength = FDCAN_DLC_BYTES_48;
	TxHeader.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
	TxHeader.BitRateSwitch = FDCAN_BRS_ON;
	TxHeader.FDFormat = FDCAN_FD_CAN;
	TxHeader.TxEventFifoControl = FDCAN_NO_TX_EVENTS;
	TxHeader.MessageMarker = 0;
	return HAL_FDCAN_AddMessageToTxFifoQ(&hfdcan, &TxHeader, &payload[0]);
}

int FD::put_to_tx(can_frame* frame){
	return put_to_tx(frame->identifier, frame->payload);
}


