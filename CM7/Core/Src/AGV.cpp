/*
 * AGV.cpp
 *
 *  Created on: Nov 28, 2023
 *      Author: maksg
 */

#include "AGV.h"
#include <cstring>
#include <stdio.h>
#include <string.h>
#include "enums.h"

AGV::AGV() : motor_1(0x0a),motor_2(0x0b),motor_3(0x0c),motor_4(0x0d), motor_5(0x0e){
	//problems were encountered where the first motor that we sent command to was always late. adding fifth, phantom motor fixed this issue
	can_frame new_frame = {0};
	motor_5.set_frame(new_frame);

}

AGV::~AGV() {
	// TODO Auto-generated destructor stub
}

void AGV::init(FDCAN_HandleTypeDef hfdcan, uint32_t ID){
	this->ID = ID;
	can_fd.bind(hfdcan);
}

int AGV::command(uint8_t* in_command){
	can_frame new_frame = {0};
	robot_command new_command = {0};
	shell_command(in_command, &new_command);

	switch(new_command.type){
		case MOTOR:
			moteus* motor_ptr;
			switch(new_command.id){
				case 0x0a:
					motor_ptr=&motor_1;
					break;
				case 0x0b:
					motor_ptr=&motor_2;
					break;
				case 0x0c:
					motor_ptr=&motor_3;
					break;
				case 0x0d:
					motor_ptr=&motor_4;
					break;
			}
			switch(new_command.mode){
				case VEL:
					motor_ptr->velocity_frame(new_command.vel, new_command.trq, &new_frame);
					motor_ptr->set_frame(new_frame);
					break;
				case STOP:
					motor_ptr->stop_frame(&new_frame);
					motor_ptr->set_frame(new_frame);
					break;
				case BRAKE:
					motor_ptr->brake_frame(&new_frame);
					motor_ptr->set_frame(new_frame);
					break;
			}
			free(motor_ptr);
			break;

		case ROBOT:
			if(new_command.id == 0x10){
				switch(new_command.mode){
					case VEL:
					{
						can_frame motor_1_frame = {0};
						can_frame motor_2_frame = {0};
						can_frame motor_3_frame = {0};
						can_frame motor_4_frame = {0};
						steer_vector vect = ikine(new_command.v_x, new_command.v_y, new_command.rot);
						motor_1.velocity_frame(vect.wrl, 0.0, &motor_1_frame);
						motor_2.velocity_frame(vect.wfl, 0.0, &motor_2_frame);
						motor_3.velocity_frame(vect.wfr, 0.0, &motor_3_frame);
						motor_4.velocity_frame(vect.wrr, 0.0, &motor_4_frame);

						motor_1.set_frame(motor_1_frame);

						motor_2.set_frame(motor_2_frame);

						motor_3.set_frame(motor_3_frame);

						motor_4.set_frame(motor_4_frame);
						break;
					}
					case STOP:
					{
						can_frame motor_1_frame = {0};
						can_frame motor_2_frame = {0};
						can_frame motor_3_frame = {0};
						can_frame motor_4_frame = {0};
						motor_1.stop_frame(&motor_1_frame);
						motor_2.stop_frame(&motor_2_frame);
						motor_3.stop_frame(&motor_3_frame);
						motor_4.stop_frame(&motor_4_frame);

						motor_1.set_frame(motor_1_frame);
						motor_2.set_frame(motor_2_frame);
						motor_3.set_frame(motor_3_frame);
						motor_4.set_frame(motor_4_frame);

						break;
					}
					case BRAKE:
					{
						can_frame motor_1_frame = {0};
						can_frame motor_2_frame = {0};
						can_frame motor_3_frame = {0};
						can_frame motor_4_frame = {0};
						motor_1.brake_frame(&motor_1_frame);
						motor_2.brake_frame(&motor_2_frame);
						motor_3.brake_frame(&motor_3_frame);
						motor_4.brake_frame(&motor_4_frame);

						motor_1.set_frame(motor_1_frame);
						motor_2.set_frame(motor_2_frame);
						motor_3.set_frame(motor_3_frame);
						motor_4.set_frame(motor_4_frame);

						break;
					}
				}

			}
			break;
		case PARAM:
			// we don't care about data, we are just looking if we should send parameters back to PC
			break;
		default: break;

	}
	return new_command.type;
}

bool AGV::shell_motor_command(uint8_t*  in_command, float* pVel, float* pTorque, uint8_t* pId){
	*pVel = 1.5;
	*pTorque = 1.5;
	*pId = 0x0b;
	return true;
}

void AGV::shell_command(uint8_t* in_command, robot_command* out_command){
	out_command->type = in_command[0];
	out_command->id = in_command[1];
	out_command->mode = in_command[2];

	if(out_command->type == MOTOR){
		memcpy(&out_command->pos,in_command+3,4);
		memcpy(&out_command->vel,in_command+7,4);
		memcpy(&out_command->trq,in_command+11,4);
		memcpy(&out_command->acc,in_command+15,4);
	}else if(out_command->type == ROBOT){
		memcpy(&out_command->v_x,in_command+3,4);
		memcpy(&out_command->v_y,in_command+7,4);
		memcpy(&out_command->rot,in_command+11,4);
	}
}

void AGV::query(int motor){
	can_frame query_frame = {0};
	if(motor == 1){
		motor_1.standard_query(&query_frame);
		if(can_fd.put_to_tx(&query_frame) != HAL_OK){
			Error_Handler();
		}
	}else if(motor == 2){
		motor_2.standard_query(&query_frame);
		if(can_fd.put_to_tx(&query_frame) != HAL_OK){
			Error_Handler();
		}
	}else if(motor == 3){
		motor_3.standard_query(&query_frame);
		if(can_fd.put_to_tx(&query_frame) != HAL_OK){
			Error_Handler();
		}
	}else if(motor == 4){
		motor_4.standard_query(&query_frame);
		if(can_fd.put_to_tx(&query_frame) != HAL_OK){
			Error_Handler();
		}
	}


}

void AGV::interpret(FDCAN_RxHeaderTypeDef head, uint8_t* in_data){
	//only standard queries are considered as of now
	uint16_t in_identifier = head.Identifier;
	//filtering frames from motors
	if(in_identifier == 0x0a10 || in_identifier == 0x0b10 || in_identifier == 0x0c10 || in_identifier == 0x0d10){
		moteus* motor_ptr;
		switch(in_identifier){
			case 0x0a10:
				motor_ptr=&motor_1;
				break;
			case 0x0b10:
				motor_ptr=&motor_2;
				break;
			case 0x0c10:
				motor_ptr=&motor_3;
				break;
			case 0x0d10:
				motor_ptr=&motor_4;
				break;
		}
		memcpy(&motor_ptr->params.mode,in_data+2,1);

		memcpy(&motor_ptr->params.position,in_data+5,4);
		memcpy(&motor_ptr->params.velocity,in_data+9,4);
		memcpy(&motor_ptr->params.torque,in_data+13,4);

		memcpy(&motor_ptr->params.power,in_data+19,4);

		memcpy(&motor_ptr->params.voltage,in_data+25,4);
		memcpy(&motor_ptr->params.board_temp,in_data+29,4);

		memcpy(&motor_ptr->params.fault,in_data+35,2);
		motor_ptr->log();
		free(motor_ptr);


	}

}

void AGV::resend_all(){

	if(motor_5.command_available){ //this is a phantom motor
		if(can_fd.put_to_tx(&motor_5.last_frame) != HAL_OK){
			Error_Handler();
		}
	}
	if(motor_3.command_available){
		if(can_fd.put_to_tx(&motor_3.last_frame) != HAL_OK){
			Error_Handler();
		}
	}

	if(motor_4.command_available){
		if(can_fd.put_to_tx(&motor_4.last_frame) != HAL_OK){
			Error_Handler();
		}
	}

	if(motor_2.command_available){
		if(can_fd.put_to_tx(&motor_2.last_frame) != HAL_OK){
			Error_Handler();
		}
	}

	if(motor_1.command_available){
		if(can_fd.put_to_tx(&motor_1.last_frame) != HAL_OK){
			Error_Handler();
		}
	}
}

steer_vector AGV::ikine(float v_x, float v_y, float rotation){
	steer_vector result;
	result.wfl = 3*10*(v_x - v_y - (0.673+0.678)*rotation)/(2*3.14159);
	result.wfr = -3*10*(v_x + v_y + (0.673+0.678)*rotation)/(2*3.14159);
	result.wrl = 3*10*(v_x + v_y - (0.673+0.678)*rotation)/(2*3.14159);
	result.wrr = -3*10*(v_x - v_y + (0.673+0.678)*rotation)/(2*3.14159);
	return result;

}
