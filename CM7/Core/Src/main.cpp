/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <cstring>
#include <stdio.h>
#include <string.h>
#include "cmsis_os.h"
#include "stdio.h"
#include "AGV.h"
#include "enums.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

#ifndef HSEM_ID_0
#define HSEM_ID_0 (0U) /* HW semaphore 0*/
#endif

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
#define USE_M4 false

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
FDCAN_HandleTypeDef hfdcan1;
UART_HandleTypeDef huart3;
UART_HandleTypeDef huart1;
AGV robot;
FDCAN_TxHeaderTypeDef   TxHeader;
FDCAN_RxHeaderTypeDef   RxHeader;
uint32_t Rx_fifo_fill_level = 0;
bool is_safe = false;

/* Definitions for UartSendTask */
osThreadId_t UartSendTaskHandle;
const osThreadAttr_t UartSendTask_attributes = {
  .name = "UartSendTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for CanSendTask */
osThreadId_t CanSendTaskHandle;
const osThreadAttr_t CanSendTask_attributes = {
  .name = "CanSendTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityAboveNormal,
};
/* Definitions for CanRecvTask */
osThreadId_t CanRecvTaskHandle;
const osThreadAttr_t CanRecvTask_attributes = {
  .name = "CanRecvTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal1,
};
/* Definitions for UartRecvTask */
osThreadId_t UartRecvTaskHandle;
const osThreadAttr_t UartRecvTask_attributes = {
  .name = "UartRecvTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityAboveNormal1,
};
/* Definitions for SafetyTask */
osThreadId_t SafetyTaskHandle;
const osThreadAttr_t SafetyTask_attributes = {
  .name = "SafetyTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityNormal1,
};

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_FDCAN1_Init(void);
void StartUartSendTask(void *argument);
void StartCanSendTask(void *argument);
void StartCanRecvTask(void *argument);
void StartUartRecvTask(void *argument);
void StartSafetyTask(void *argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
bool new_uart_data = false;
bool send_diagnostic = false;
uint8_t uart_data_rx[19];
uint32_t last_frame_t;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	last_frame_t = HAL_GetTick();
	new_uart_data = true;
	HAL_GPIO_TogglePin(yellow_led_GPIO_Port, yellow_led_Pin);
	int type_of_command = robot.command(uart_data_rx);
	if(type_of_command == PARAM){
		send_diagnostic = true;
	}

	HAL_UART_Receive_IT(&huart3, uart_data_rx, 19); //initalizing UART interrupt again
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
    /* USER CODE BEGIN 1 */

    /* USER CODE END 1 */
	/* USER CODE BEGIN Boot_Mode_Sequence_0 */
	int32_t timeout;
	/* USER CODE END Boot_Mode_Sequence_0 */

	/* USER CODE BEGIN Boot_Mode_Sequence_1 */
	/* Wait until CPU2 boots and enters in stop mode or timeout*/
	if(USE_M4){
		timeout = 0xFFFF;
		while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) != RESET) && (timeout-- > 0));
		if ( timeout < 0 ){
		Error_Handler();
		}
	}
	/* USER CODE END Boot_Mode_Sequence_1 */
    /* MCU Configuration--------------------------------------------------------*/

    /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
    HAL_Init();

    /* USER CODE BEGIN Init */

    /* USER CODE END Init */
    /* Configure the system clock */
    SystemClock_Config();
	/* USER CODE BEGIN Boot_Mode_Sequence_2 */
    if(USE_M4){
		/* When system initialization is finished, Cortex-M7 will release Cortex-M4 by means of
		HSEM notification */
		/*HW semaphore Clock enable*/
		__HAL_RCC_HSEM_CLK_ENABLE();
		/*Take HSEM */
		HAL_HSEM_FastTake(HSEM_ID_0);
		/*Release HSEM in order to notify the CPU2(CM4)*/
		HAL_HSEM_Release(HSEM_ID_0,0);
		/* wait until CPU2 wakes up from stop mode */
		timeout = 0xFFFF;
		while((__HAL_RCC_GET_FLAG(RCC_FLAG_D2CKRDY) == RESET) && (timeout-- > 0));
		if ( timeout < 0 )
		{
		Error_Handler();
		}
    }
	/* USER CODE END Boot_Mode_Sequence_2 */

    /* USER CODE BEGIN SysInit */

    /* USER CODE END SysInit */

    /* Initialize all configured peripherals */
    MX_GPIO_Init();
    MX_USART3_UART_Init();
    MX_FDCAN1_Init();
    /* USER CODE BEGIN 2 */
    if(HAL_FDCAN_Start(&hfdcan1)!= HAL_OK)
      {
    	  Error_Handler();
      }
    robot.init(hfdcan1, 0x10);
    /* USER CODE END 2 */

    /* Init scheduler */
    osKernelInitialize();

    /* USER CODE BEGIN RTOS_MUTEX */
    /* add mutexes, ... */
    /* USER CODE END RTOS_MUTEX */

    /* USER CODE BEGIN RTOS_SEMAPHORES */
    /* add semaphores, ... */
    /* USER CODE END RTOS_SEMAPHORES */

    /* USER CODE BEGIN RTOS_TIMERS */
    /* start timers, add new ones, ... */
    /* USER CODE END RTOS_TIMERS */

    /* USER CODE BEGIN RTOS_QUEUES */
    /* add queues, ... */
    /* USER CODE END RTOS_QUEUES */

    /* Create the thread(s) */
    /* creation of UartSendTask */
    UartSendTaskHandle = osThreadNew(StartUartSendTask, NULL, &UartSendTask_attributes);

    /* creation of CanSendTask */
    CanSendTaskHandle = osThreadNew(StartCanSendTask, NULL, &CanSendTask_attributes);

    /* creation of CanRecvTask */
    CanRecvTaskHandle = osThreadNew(StartCanRecvTask, NULL, &CanRecvTask_attributes);

    /* creation of UartRecvTask */
    UartRecvTaskHandle = osThreadNew(StartUartRecvTask, NULL, &UartRecvTask_attributes);

    /* creation of SafetyTask */
    SafetyTaskHandle = osThreadNew(StartSafetyTask, NULL, &SafetyTask_attributes);

    /* USER CODE BEGIN RTOS_THREADS */
    /* add threads, ... */
    /* USER CODE END RTOS_THREADS */

    /* USER CODE BEGIN RTOS_EVENTS */
    /* add events, ... */
    /* USER CODE END RTOS_EVENTS */

    /* Start scheduler */
    osKernelStart();

    /* We should never get here as control is now taken by the scheduler */
    /* Infinite loop */
    /* USER CODE BEGIN WHILE */
    while (1){
    	/* USER CODE END WHILE */

    	/* USER CODE BEGIN 3 */
    }
    /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_DIRECT_SMPS_SUPPLY);

  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 50;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLQ = 16;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_3;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV2;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV2;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief FDCAN1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_FDCAN1_Init(void)
{

  /* USER CODE BEGIN FDCAN1_Init 0 */

  /* USER CODE END FDCAN1_Init 0 */

  /* USER CODE BEGIN FDCAN1_Init 1 */

  /* USER CODE END FDCAN1_Init 1 */
  hfdcan1.Instance = FDCAN1;
  hfdcan1.Init.FrameFormat = FDCAN_FRAME_FD_NO_BRS;
  hfdcan1.Init.Mode = FDCAN_MODE_NORMAL;
  hfdcan1.Init.AutoRetransmission = DISABLE;
  hfdcan1.Init.TransmitPause = DISABLE;
  hfdcan1.Init.ProtocolException = DISABLE;
  hfdcan1.Init.NominalPrescaler = 1;
  hfdcan1.Init.NominalSyncJumpWidth = 16;
  hfdcan1.Init.NominalTimeSeg1 = 56;
  hfdcan1.Init.NominalTimeSeg2 = 28;
  hfdcan1.Init.DataPrescaler = 1;
  hfdcan1.Init.DataSyncJumpWidth = 5;
  hfdcan1.Init.DataTimeSeg1 = 11;
  hfdcan1.Init.DataTimeSeg2 = 5;
  hfdcan1.Init.MessageRAMOffset = 0;
  hfdcan1.Init.StdFiltersNbr = 0;
  hfdcan1.Init.ExtFiltersNbr = 0;
  hfdcan1.Init.RxFifo0ElmtsNbr = 64;
  hfdcan1.Init.RxFifo0ElmtSize = FDCAN_DATA_BYTES_48;
  hfdcan1.Init.RxFifo1ElmtsNbr = 0;
  hfdcan1.Init.RxFifo1ElmtSize = FDCAN_DATA_BYTES_8;
  hfdcan1.Init.RxBuffersNbr = 0;
  hfdcan1.Init.RxBufferSize = FDCAN_DATA_BYTES_8;
  hfdcan1.Init.TxEventsNbr = 0;
  hfdcan1.Init.TxBuffersNbr = 0;
  hfdcan1.Init.TxFifoQueueElmtsNbr = 32;
  hfdcan1.Init.TxFifoQueueMode = FDCAN_TX_FIFO_OPERATION;
  hfdcan1.Init.TxElmtSize = FDCAN_DATA_BYTES_48;
  if (HAL_FDCAN_Init(&hfdcan1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN FDCAN1_Init 2 */
//    FDCAN_FilterTypeDef sFilterConfig;
//
//    sFilterConfig.IdType = FDCAN_EXTENDED_ID;
//    sFilterConfig.FilterIndex = 0;
//    sFilterConfig.FilterType = FDCAN_FILTER_MASK;
//    sFilterConfig.FilterConfig = FDCAN_FILTER_TO_RXFIFO0;
//    sFilterConfig.FilterID1 = 0x1011;
//    sFilterConfig.FilterID2 = 0x1011;
//    sFilterConfig.RxBufferIndex = 0;
//    if (HAL_FDCAN_ConfigFilter(&hfdcan1, &sFilterConfig) != HAL_OK){
//      /* Filter configuration Error */
//      Error_Handler();
//    }
  /* USER CODE END FDCAN1_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.Init.ClockPrescaler = UART_PRESCALER_DIV1;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetTxFifoThreshold(&huart3, UART_TXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_SetRxFifoThreshold(&huart3, UART_RXFIFO_THRESHOLD_1_8) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_UARTEx_DisableFifoMode(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(green_led_GPIO_Port, green_led_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(yellow_led_GPIO_Port, yellow_led_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : green_led_Pin */
  GPIO_InitStruct.Pin = green_led_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(green_led_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : yellow_led_Pin */
  GPIO_InitStruct.Pin = yellow_led_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(yellow_led_GPIO_Port, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartUartSendTask */
/**
  * @brief  Function implementing the UartSendTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartUartSendTask */
void StartUartSendTask(void *argument)
{
	/* USER CODE BEGIN 5 */
	uint8_t TxData[109] = {0};
	/* Infinite loop */
	for(;;){
		if(send_diagnostic){
			TxData[0] = 0x01;
			motor_param* motor_1_parameters = &robot.motor_1.params;
			motor_param* motor_2_parameters = &robot.motor_2.params;
			motor_param* motor_3_parameters = &robot.motor_3.params;
			motor_param* motor_4_parameters = &robot.motor_4.params;
			memcpy(&TxData[1],motor_1_parameters,27);
			memcpy(&TxData[28],motor_2_parameters,27);
			memcpy(&TxData[55],motor_3_parameters,27);
			memcpy(&TxData[82],motor_4_parameters,27);
			HAL_UART_Transmit(&huart3, TxData, 109, 100);
			send_diagnostic = false;
		}

    osDelay(100);
	}
	/* USER CODE END 5 */
}

/* USER CODE BEGIN Header_StartCanSendTask */
/**
* @brief Function implementing the CanSendTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartCanSendTask */
void StartCanSendTask(void *argument)
{
  /* USER CODE BEGIN StartCanSendTask */
	//used for querying drivers to avoid timeout
	uint32_t last_resend = HAL_GetTick();
	int query_index = 0;
  /* Infinite loop */
  for(;;)
  {
	robot.query(query_index++);
	if(query_index > 4) query_index = 0;
	if(HAL_GetTick() - last_resend > 50 && is_safe){
		robot.resend_all();
		last_resend = HAL_GetTick();
//		HAL_GPIO_TogglePin(green_led_GPIO_Port, green_led_Pin);
	}
    osDelay(20);
  }
  /* USER CODE END StartCanSendTask */
}

/* USER CODE BEGIN Header_StartCanRecvTask */
/**
* @brief Function implementing the CanRecvTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartCanRecvTask */
void StartCanRecvTask(void *argument)
{
  /* USER CODE BEGIN StartCanRecvTask */
	uint8_t RxData[48] = {0};
  /* Infinite loop */
  for(;;)
  {
	Rx_fifo_fill_level = HAL_FDCAN_GetRxFifoFillLevel(&hfdcan1, FDCAN_RX_FIFO0); //number of messages in RX FIFO. max=64
	if(Rx_fifo_fill_level>0){
		if (HAL_FDCAN_GetRxMessage(&hfdcan1, FDCAN_RX_FIFO0, &RxHeader, RxData) != HAL_OK){
			/* Reception Error */
			Error_Handler();
		}
		robot.interpret(RxHeader,RxData);
	}
    osDelay(1);
  }
  /* USER CODE END StartCanRecvTask */
}

/* USER CODE BEGIN Header_StartUartRecvTask */
/**
* @brief Function implementing the UartRecvTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartUartRecvTask */
void StartUartRecvTask(void *argument)
{
	/* USER CODE BEGIN StartUartRecvTask */
	HAL_UART_Receive_IT(&huart3, uart_data_rx, 19); //initalizing UART interrupt
	new_uart_data = false;
	/* Infinite loop */
	for(;;){
//	if(new_uart_data){
//		HAL_GPIO_TogglePin(yellow_led_GPIO_Port, yellow_led_Pin);
//		int type_of_command = robot.command(uart_data_rx);
//		if(type_of_command == PARAM){
//			send_diagnostic = true;
//		}
//
//		HAL_UART_Receive_IT(&huart3, uart_data_rx, 19); //initalizing UART interrupt again
//		new_uart_data = false;
//	}
    osDelay(10);
  }
  /* USER CODE END StartUartRecvTask */
}

/* USER CODE BEGIN Header_StartSafetyTask */
/**
* @brief Function implementing the SafetyTask thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartSafetyTask */
void StartSafetyTask(void *argument)
{
  /* USER CODE BEGIN StartSafetyTask */
	uint32_t max_frame_t = 500;
  /* Infinite loop */
  for(;;)
  {
	  if(HAL_GetTick() - last_frame_t > 500){ /* frame not received*/
		  is_safe = false;
	  } else {
		  is_safe = true;
	  }

	  if(is_safe){
		  HAL_GPIO_WritePin(green_led_GPIO_Port, green_led_Pin, GPIO_PIN_SET);
	  }else{
		  HAL_GPIO_WritePin(green_led_GPIO_Port, green_led_Pin, GPIO_PIN_RESET);
	  }

    osDelay(10);
  }
  /* USER CODE END StartSafetyTask */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
